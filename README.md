# Convex Hull Web App

[Presentation](https://youtu.be/-eTDJBwQSr0)

## Installing Golang
[Golang Install Tutorial](https://golang.org/doc/install)

## Running
### Runing with docker 
```
cd docker/back/
docker build -t backendv1 .
cd ../front/
docker build -t frontendv1 .
cd ..
docker-compose up -d
```

### Running locally
```
// in one terminal
GOPATH=$(pwd):~/go go build
./convex-hull-web-app

// in another terminal
cd ui/
npm install && npm start
```

## Testing
```bash
$ curl -X POST http://localhost:8080/upload \
       -F "file=@/home/n1mda/Documents/cooper/ece465-final/sample.txt" \
       -H "Content-Type: multipart/form-data"
```
