package main

import (
	"algorithm"
	"github.com/gin-gonic/gin"
	"net/http"
	"os/exec"
	"path/filepath"
)

//var ginLambda *ginadapter.GinLambda
//var s3Client *s3.Client
//var BUCKET = "cooper-union-ece465-final-v1"

// SaveFileHandler : handles a POST form request for file
// https://ramezanpour.net/post/2020/09/12/file-upload-using-go-gin

func upload3d(c *gin.Context) {
	file, err := c.FormFile("file")

	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
			"message": "no file is received",
		})
		return
	}

	newFileName := file.Filename
	htmlFileName := newFileName[0:len(newFileName)-len(filepath.Ext(newFileName))] + ".html"

	if err := c.SaveUploadedFile(file, "./tmp/" + newFileName); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H {
			"message": "unable to save file",
		})
		return
	}

	// from testAlgorithm.go
	algorithm.RunQuickHull3(newFileName, "done-" + newFileName)
	exec.Command("python3", "plotter/graph.py", "5", "tmp/" + newFileName, "tmp/" + "done-" + newFileName)

	c.JSON(http.StatusOK, gin.H {
		"message": "file successfully uploaded",
		"filename": "done-" + newFileName,
		"html": "done-" + htmlFileName,
	})
}

func upload2d(c *gin.Context) {
	file, err := c.FormFile("file")

	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H {
			"message": "no file is received",
		})
		return
	}

	newFileName := file.Filename
	htmlFileName := newFileName[0:len(newFileName)-len(filepath.Ext(newFileName))] + ".html"

	//uploaded locally
	if err := c.SaveUploadedFile(file, "./tmp/" + newFileName); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H {
			"message": "unable to save file",
		})
		return
	}

	// from testAlgorithm.go
	algorithm.RunQuickHull(newFileName, "done-" + newFileName)
	exec.Command("python3", "plotter/graph.py", "4", "tmp/" + newFileName, "tmp/" + "done-" + newFileName)

	c.JSON(http.StatusOK, gin.H {
		"message": "file successfully uploaded",
		"filename": "done-" + newFileName,
		"html": "done-" + htmlFileName,
	})
}

func ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H {
		"message": "it works",
	})
}

func serveFile(c *gin.Context) {
	filename := c.Param("filename")
	c.Set("content-type", "application/force-download")
	c.Header("Content-Disposition", "attachment; filename="+ filename)
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}
		c.Next()
	}
}

func main() {
	//lambda.Start(Handler)
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()

	router.Use(CORSMiddleware())

	router.Static("/files", "./tmp")
	//router.GET("/files/:filename", serveFile)

	router.GET("/pingme", ping)

	router.POST("/upload2d", upload2d)
	router.POST("/upload3d", upload3d)

	router.Run(":8080")
}