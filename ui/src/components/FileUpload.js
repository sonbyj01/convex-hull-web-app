import React, { useState, useEffect } from "react";
// import UploadService from "../services/FileUploadService";

import http from "../http-common";

const UploadFiles = () => {
    const [selectedFiles, setSelectedFiles] = useState(undefined);
    const [currentFile, setCurrentFile] = useState(undefined);
    const [download, setDownload] = useState(undefined);
    const [uploadedFile, setUploadedFile] = useState(undefined);
    const [message, setMessage] = useState("");
    const [html, setHtml] = useState("");


    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
        setUploadedFile(undefined);
    };

    const test = () => {
        setCurrentFile(selectedFiles[0]);
        var formData = new FormData();
        formData.append("file", currentFile);
        http.post("/upload2d", formData, {
            headers: {
                "Content-Type": "multipart/form-data",
            }
        }).then((response) => {
            setDownload(response.data.filename);
            setHtml(response.data.html);
        });
        setUploadedFile(currentFile);
    }

    const test3 = () => {
        setCurrentFile(selectedFiles[0]);
        var formData = new FormData();
        formData.append("file", currentFile);
        http.post("/upload3d", formData, {
            headers: {
                "Content-Type": "multipart/form-data",
            }
        }).then((response) => {
            setDownload(response.data.filename);
            setHtml(response.data.html);
        });
        setUploadedFile(currentFile);
    }

    const getDoneFile = () => {
        http.get("/files/" + download, {
            params: {
                responseType: 'blob'
            }
        }).then((response) => {
            const url = window.URL
                .createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'convexhull.txt');
            document.body.appendChild(link);
            link.click();
        });
    }

    const viewDoneFile = () => {
        // window.open("localhost:8080/files/" + html)
        http.get("/files/" + html, {
            params: {
                responseType: 'blob'
            }
        }).then((response) => {
            const url = window.URL
                .createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'convexhull.html');
            document.body.appendChild(link);
            link.click();
        });
    }

    return (
        <div>
            <label className="btn btn-default">
                <input type="file" onChange={selectFile} />
            </label>

            <button
                className="btn btn-success"
                disabled={!selectedFiles}
                onClick={test}
            >
                Upload 2D
            </button>

            <button
                className="btn btn-success"
                disabled={!selectedFiles}
                onClick={test3}
            >
                Upload 3D
            </button>

            <button
                className="btn btn-success"
                disabled={!selectedFiles || !uploadedFile}
                onClick={getDoneFile}
            >
                Download Convex
            </button>

            <button
                className="btn btn-success"
                disabled={!selectedFiles || !uploadedFile}
                onClick={viewDoneFile}
            >
                Download Interactive HTML Convex
            </button>

            <div className="alert alert-light" role="alert">
                {message}
            </div>

            {/*<div className="App">*/}
            {/*    <header className="App-header">*/}
            {/*    <Plot*/}
            {/*        data={[*/}
            {/*        {*/}
            {/*            x: {xOrig},*/}
            {/*            y: {yOrig},*/}
            {/*            type: 'scatter',*/}
            {/*            mode: 'markers',*/}
            {/*            marker: {color: 'red'},*/}
            {/*        },*/}
            {/*        {type: 'markers', x: {xConvex}, y: {yConvex}},*/}
            {/*        ]}*/}
            {/*        layout={ {width: 320, height: 240, title: 'A Fancy Plot'} }*/}
            {/*    />*/}
            {/*    </header>*/}
            {/*</div>*/}
        </div>
    );
};

export default UploadFiles;